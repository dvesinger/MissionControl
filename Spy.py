from urllib.request import urlopen
from html.parser import HTMLParser

class Spy(HTMLParser):
    
    def __init__(self, name):
        HTMLParser.__init__(self)
        self.name = name
        self.links = []
        self.info = []
        self.isParagraph = 0

    def handle_starttag(self, tag, attrs):
        if tag == "p":
            self.isParagraph = 1
                
        elif tag == "a":
                for i in attrs:
                    if i[0]=="href":
                        if "en.wikipedia" in i[1] or "wiki" in i[1] and "tools.wmf" not in i[1]:
                            if i[1].startswith("https://"):
                                self.links.append(i[1])
                                self.url_num +=1
                            elif i[1].startswith("//"):
                                self.links.append("https:" + i[1])
                                self.url_num +=1
                            elif i[1].startswith("/"):
                                self.links.append("https://en.wikipedia.org" + i[1])
                                self.url_num +=1

    def handle_endtag(self, tag):
        if tag == "p":
            self.isParagraph = 0

    def handle_data(self, data):
        if self.isParagraph == 1:
            if self.name in data:
                self.info.append(data)
                
    def unknown_decl(self, data):
        print(data)

    def error(self, message):
        pass
            

def main():
    secret_name = input("Unesi ime: ")
    hop = 1
    visited = []
    page = str(urlopen("https://en.wikipedia.org/wiki/Special:Random").read())
    name_spy = Spy(secret_name)
    name_spy.feed(page)

    while len(name_spy.info)==0 and len(name_spy.links) > 0:
        for link in name_spy.links:            
            if link not in visited:
                print(link)
                visited.append(link)
                try:
                    name_spy.url_num = 0
                    name_spy.feed(str(urlopen(link).read()))
                except :
                    pass
            if(len(name_spy.info)>0):
                break
            
    print(name_spy.info)

if __name__=="__main__":
    main()
